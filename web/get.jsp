<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="servlet.Servlet"%>
<html>
<head>
    <title>Assistants</title>
</head>
<body>
<h3>Assistants</h3>
<% pageContext.setAttribute("assistants", Servlet.getAssistants());%>
    <core:if test="${empty assistants}">
        <p>Assistant list is empty</p>
    </core:if>

    <ol>
        <c:forEach var="assistant" items="${assistants}">
            <li>${assistant.name} ${assistant.calculateAverageScore()}</li>
        </c:forEach>
    </ol>
</body>
</html>
