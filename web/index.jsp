<%@ page contentType="text/html;charset=UTF-8" %>
<html>
  <head>
    <title>Assistant</title>
  </head>
  <body>
    <div id="page-wrap">
      <form method="post" action="save">
        <label for="name">Name:</label>
        <input type="text" name="name" id="name" required/>

        <label for="score">Score:</label>
        <input type="number" name="score" id="score" required/>

        <input type="submit" name="submit" value="Submit"/>
      </form>
      <div style="clear: both;"></div>
      <form method="get" action="get">
        <input type="submit" name="list" value="List assistants">
      </form>
    </div>
  </body>
</html>
