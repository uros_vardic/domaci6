package servlet;

import model.Assistant;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Servlet extends HttpServlet {

    private static final List<Assistant> assistants = new ArrayList<>();

    public static List<Assistant> getAssistants() {
        return assistants;
    }

    @Override
    protected synchronized void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        final String name = request.getParameter("name");

        final int score = Integer.parseInt(request.getParameter("score"));

        final Assistant assistant = new Assistant(name, score);

        if (assistants.contains(assistant)) {
            Assistant existing = assistants.get(assistants.indexOf(assistant));
            existing.setScore(existing.getScore() + score);
            existing.incrementNumberOfEntries();
        } else
            assistants.add(assistant);

        final PrintWriter out = response.getWriter();

        response.setContentType("text/html");
        out.println("<body><h1>Entry saved</h1></body>");
        out.close();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final RequestDispatcher dispatcher = request.getRequestDispatcher("/get.jsp");

        dispatcher.forward(request, response);
    }
}
