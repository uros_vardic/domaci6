package model;

import java.util.Objects;

public class Assistant {

    private final String name;

    private int score;

    private int numberOfEntries;

    public Assistant(String name, int score) {
        this.name = name.toUpperCase().trim();

        if (this.name.equals("VUK"))
            this.score = 0;
        else
            this.score = score;

        numberOfEntries++;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void incrementNumberOfEntries() {
        numberOfEntries++;
    }

    public int calculateAverageScore() {
        return score / numberOfEntries;
    }

    @Override
    public String toString() {
        return String.format("Assistant:[name=%s, score=%d, numberOfEntries=%d]", name, score, numberOfEntries);
    }

    @Override
    public boolean equals(Object reference) {
        if (reference instanceof Assistant) {
            final Assistant other = (Assistant) reference;

            return Objects.equals(this.name, other.name);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name);
    }
}
